<p>Once you install the app, Click on <b><a href="https://<?php echo $_SERVER['HTTP_HOST'] ?><?php echo $dashboard_route; ?>" target ="_blank"><?php echo $app_type; ?></a></b> Tab. <?php echo $app_type; ?> tab has two options : <b><?php echo $app_type_singular; ?> Setting</b> and <b><?php echo $app_type_singular; ?> List</b>
            <ul class="ul-help">
                <div class ="row">
                    <div class ="col-sm-6">
                        <p><b><?php echo $app_type; ?> setting</b></p>
                        <ul>
                            <li>Click on Add fields button and add the Custom Fields According to Your Requirement.</li>
                            <li>Click on save button.</li>
                            <li>Dynamic shortcode will be generated for each of the fields added.</li>
                        </ul>
                    </div>

                    <div class ="col-sm-6">
                        <div class ="screenshot_box">
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_002.png') }}" target="_blank">
                                <img class="img-responsive" src="{{ asset('image/help/help_002.png') }}">
                            </a>
                        </div>
                    </div>
                </div>
                <br>
</ul>

           