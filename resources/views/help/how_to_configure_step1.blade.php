 <ul>
                <li>
                      <p>Copy the shortcode from below and paste in <b><a href="https://<?php echo $store_name; ?><?php echo $liquid_file_path ;?>" target ="_blank"><?php echo $app_type_singular.".liquid"; ?></a></b> file.</p>
                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button id ='main_shortcode_button' type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_collection_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>

                            <textarea id="shortcode_collection_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo $main_shortcode ?></textarea>
                        </div>
                    </div>

                    <!--                    <div class="form-group">
                                            <div class="showCodeWrapperarea">
                                                <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_collection_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                                                <textarea id="shortcode_collection_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo $field_shortcode; ?> </textarea></div>
                                        </div> -->
                </li>

                <li>
                    Here, "Your short-code" referes to, the dynamic shortcode generated for each of the form fields added.
                </li>
</ul>