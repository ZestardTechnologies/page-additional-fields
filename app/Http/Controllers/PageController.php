<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class PageController extends Controller {

    public function index() {

        $sh = App::make('ShopifyAPI');
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $Fields = DB::table('zestard_page_field')->where('store_id', $select_store[0]->id)->get();
        return view('tab_pages.pages', ['Fields' => $Fields]);
    }

    public function get_pages(Request $request) {

        $sh = App::make('ShopifyAPI');
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $count = $sh->call(['URL' => '/admin/pages/count.json', 'METHOD' => 'GET']);
        $custom_count = $count->count;

        if ($custom_count > 0) {

            $limit = $request['length'];
            $draw = $request['draw'];
            $start = $request['start'];
            $search = $request['search']['value'];
            $total_pages = array('draw' => $draw, 'recordsTotal' => 0, 'recordsFiltered' => 0);
            $val = $start + 1;
            if ($search) {

                $pages = ceil($custom_count / 250);
                $limit = 250;
                for ($i = 0; $i < $pages; $i++) {
                    $current_page = $i + 1;
                    $custom_pages = $sh->call(['URL' => '/admin/pages.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                    foreach ($custom_pages->pages as $pages) {
                        if (stristr($pages->title, $search)) {
                            $link = 'edit_page/' . $pages->id;
                            $total_pages['data'][] = array($val, $pages->id, $pages->title, $link);
                        }
                        $val++;
                    }
                    $total_pages['recordsTotal'] = $custom_count;
                    $total_pages['recordsFiltered'] = $custom_count;
                }
            } else {

                $current_page = ceil($start / $limit) + 1;
                $custom_pages = $sh->call(['URL' => '/admin/pages.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                foreach ($custom_pages->pages as $pages) {
                    $link = 'edit_page/' . $pages->id;
                    $total_pages['data'][] = array($val, $pages->id, $pages->title, $link);
                    $val++;
                }
                $total_pages['recordsTotal'] = $custom_count;
                $total_pages['recordsFiltered'] = $custom_count;
            }

            return json_encode($total_pages);
        } else {
            $total_pages = array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0);
            $total_pages['data'] = array();
            return json_encode($total_pages);
        }
    }

    public function edit_page($page_id = '') {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $count = $sh->call(['URL' => '/admin/pages/count.json', 'METHOD' => 'GET']);
        $custom_count = $count->count;
        if ($custom_count > 0) {

            $custom_pages = $sh->call(['URL' => 'admin/pages/' . $page_id, 'METHOD' => 'GET']);
            $output['store_id'] = $select_store[0]->id;
            $output['page_id'] = $custom_pages->page->id;
            $output['title'] = $custom_pages->page->title;
            if (isset($custom_pages->page->image->src) && $custom_pages->page->image->src != '') {
                $output['image'] = $custom_pages->page->image->src;
            }
            $ExistRecord = DB::table('zestard_page_field')->where('store_id', $select_store[0]->id)->get();
            foreach ($ExistRecord as $key => $value) {
                $field_id = $value->field_id;
                $output['fields'][] = $value;
            }
            return view('tab_pages.add_pages', $output);
        }
    }

    public function page_settings_saveform(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $storeid = $select_store[0]->id;

        if (isset($request['contact']['deletefieldsids']) && $request['contact']['deletefieldsids'] != '') {
            $fieldsIds = $request['contact']['deletefieldsids'];
            $fieldIds = explode(',', $fieldsIds);
            $deleteRecord1 = DB::table('zestard_page_field')->whereIn('field_id', $fieldIds)->delete();
            $deleteRecord2 = DB::table('page_customize_table')->whereIn('field_id', $fieldIds)->delete();
        }

        $Fields = isset($request['contact']['fields']) ? $request['contact']['fields'] : '';
        if (isset($Fields) && $Fields != '') {

            foreach ($Fields as $key => $Field) {
                $Field['store_id'] = $select_store[0]->id;
                if (isset($Field["title"]) && $Field["title"] != '') {
                    $Field["title"] = trim($Field["title"]);
                    $Field["title"] = $this->alpha_dash_space_allw_space($Field["title"]);
                }
                if (isset($Field['values']) && !empty($Field['values'])) {
                    $array = array();
                    foreach ($Field['values'] as $key2 => $value2) {
                        if (trim($value2['title']) != '') {
                            $array[]['title'] = $value2['title'];
                        }
                    }
                    if (!empty($array)) {
                        $Field['child_options'] = serialize(($array));
                    }
                    unset($Field['values']);
                }
                if (isset($Field['field_id']) && $Field['field_id'] > 0) {
                    DB::table('zestard_page_field')->where('field_id', $Field['field_id'])->update($Field);
                } else {
                    //$Field['shortcode_title'] = 'shortcode_page_' . strtolower($this->alpha_dash_space_not_allow_space($Field["title"])) . '_' . $Field['type'];
                    $Field['shortcode_title'] = 'shortcode_page_' . strtolower($this->alpha_dash_space_not_allow_space($Field["title"])) . '_title';
                    $Field['shortcode_value'] = 'shortcode_page_' . strtolower($this->alpha_dash_space_not_allow_space($Field["title"])) . '_value';
                    DB::table('zestard_page_field')->insert($Field);
                }
            }
        }
        $notification = array(
            'message' => 'Your changes has been store Successfully',
            'alert-type' => 'success'
        );
        return redirect('pages')->with('notification', $notification);
    }

    public function update_page($page_id = '', Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $custom = array();
        $data = array();
        $custom = $request['custom'];
        $data['store_id'] = $select_store[0]->id;
        $warningCount = 0;
        $errors = '';
        if (isset($_FILES)) {
            foreach ($_FILES as $key => $file) {
                $FieldId = (int) str_replace('fields', '', $key);
                $field = DB::table('zestard_page_field')->where([['field_id', '=', $FieldId]])->first();
                $file_name = 'fields' . $FieldId;
                $fileSize = $field->file_size * 1024;
                $this->validate($request, [$key => 'image|mimes:' . $field->file_extension . '|max:' . $fileSize], [$key . '.mimes' => 'Please upload only ' . $field->file_extension . ' file types'], [$key . '.image' => 'Please upload image']);


                $queryFields = DB::table('page_customize_table')->where([['field_id', '=', $FieldId], ['page_id', '=', "" . $custom[$FieldId]['PageId'] . ""]])->first();

                if ($_FILES[$key]['size'] > 0 && $_FILES[$key]['name'] != '') {
                    if ($request->hasFile($file_name)) {
                        $app_icon = time() . "1" . '.' . $request->$file_name->getClientOriginalExtension();
                        $request->$file_name->move(public_path('uploads/mailed/'), $app_icon);
                    }
                    $custom[$FieldId]['values'] = $app_icon;
                    if (count($queryFields) > 0 && $queryFields->values != '') {
                        $upoaded_path_image = 'uploads/mailed/' . $queryFields->values;
                        if (file_exists($upoaded_path_image)) {
                            unlink($upoaded_path_image);
                        }
                    }
                } else {

                    if ($_FILES[$key]['size'] == 0 && $_FILES[$key]['name'] == '') {
                        if (count($queryFields) > 0 && $queryFields->values != '') {
                            $custom[$FieldId]['values'] = $queryFields->values;
                        } else {
                            $custom[$FieldId]['values'] = '';
                        }
                    }
                }
            }
        }


        if ($custom != '') {
            foreach ($custom as $key => $value) {


                if (isset($data['page_id']) && $data['page_id'] != '') {
                    $PageId = $data['page_id'];
                }
                $data['page_id'] = $value['PageId'];
                $data['field_id'] = $key;
                if (isset($value['values']) && is_array($value['values'])) {
                    $data['values'] = implode(',', $value['values']);
                } else if (isset($value['values']) && $value['values'] != '') {
                    $data['values'] = $value['values'];
                } else {
                    $data['values'] = '';
                }
                $query = DB::table('page_customize_table')->where([['field_id', '=', $data['field_id']], ['page_id', '=', "" . $data['page_id'] . ""]])->get();

                if (count($query) > 0) {

                    DB::table('page_customize_table')->where([['field_id', '=', $data['field_id']], ['page_id', '=', "" . $data['page_id'] . ""]])->update(array('values' => $data['values']));
                    $notification = array(
                        'message' => 'Your page updated successfully',
                        'alert-type' => 'success'
                    );
                } else {

                    if (isset($data) && !empty($data)) {
                        DB::table('page_customize_table')->insert($data);
                        $notification = array(
                            'message' => 'Your page added successfully',
                            'alert-type' => 'success'
                        );
                    }
                }
            }
        }
        return redirect('edit_page/' . $page_id)->with('notification', $notification);
    }

    public function alpha_dash_space_allw_space($string) {
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function alpha_dash_space_not_allow_space($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function get_single_pagedata(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $shop = $request['shop'];
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $shortcode_title = $_GET['shortcode_title'];
        $pageid = $_GET['pageid'];
        //$shop = $_GET['shop'];
        $shopId = $select_store[0]->id;

        $getView = array();
        $data = array();
        $global_settings = DB::table('zestard_global_config')->where('store_id', $shopId)->first();
        $app_status = $global_settings->app_status;
        $data = $this->get_fieldsValues_on_page($shortcode_title, $pageid, $shopId, $app_status);

        if ($data != 'error' && $data != 'err') {
            foreach ($data as $key => $value) {

                if ($value->type == 'file') {
                    $file = url('uploads/mailed/' . $value->values);
                    if (isset($value->height) && $value->height != '' && isset($value->width) && $value->width != '') {
                        $height = $value->height;
                        $width = $value->width;
                    } else {
                        $height = '100';
                        $width = '100';
                    }
                    $data[$key]->values = '<img src=' . $file . ' height=' . $height . ' width=' . $width . '>';
                }
                if ($value->type == 'video') {
                    $url = $value->values;
                    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                    $id = $matches[1];
                    if (isset($value->height) && $value->height != '' && isset($value->width) && $value->width != '') {
                        $height = $value->height;
                        $width = $value->width;
                    } else {
                        $height = '150';
                        $width = '250';
                    }
                    $data[$key]->values = '<iframe id="ytplayer" type="text/html" width="' . $width . '" height="' . $height . '"
    src="https://www.youtube.com/embed/' . $id . '?rel=0&showinfo=0&color=white&iv_load_policy=3"
    frameborder="0" allowfullscreen></iframe>';
                }
                if ($value->type == 'multiple' || $value->type == 'checkbox') {
                    if ($value->child_options != '') {
                        $multiple_ids = explode(',', $value->values);
                        $child_options = unserialize($value->child_options);
                        $data[$key]->values = array();
                        foreach ($child_options as $key2 => $value2) {
                            if (in_array($key2 + 1, $multiple_ids)) {
                                $data[$key]->values[] = $value2['title'];
                            }
                        }
                        $data[$key]->values = implode(',', $data[$key]->values);
                    }
                }

                if ($value->type == 'field' || $value->type == 'radio' || $value->type == 'drop_down' || $value->type == 'area' || $value->type == 'time' || $value->type == 'date' || $value->type == 'date_time') {
                    if ($value->child_options != '') {
                        $child_options = unserialize($value->child_options);
                        foreach ($child_options as $key2 => $value2) {
                            if ($value->values == $key2 + 1) {
                                $data[$key]->values = $value2['title'];
                            }
                        }
                    }
                }
                if ($value->type == 'field' || $value->type == 'radio' || $value->type == 'drop_down' || $value->type == 'area' || $value->type == 'time' || $value->type == 'date' || $value->type == 'date_time') {
                    if ($value->child_options != '') {
                        $child_options = unserialize($value->child_options);
                        foreach ($child_options as $key2 => $value2) {
                            if ($value->values == $key2 + 1) {
                                $data[$key]->values = $value2['title'];
                            }
                        }
                    }
                } else {
                    $value->new_option = '';
                }
            }
        }
        echo json_encode($data);
    }

    public function get_fieldsValues_on_page($shortcode_title, $pageid, $shopId, $app_status) {
        if (isset($shopId) && $shopId != '' && $app_status == '1') {
            if (isset($shortcode_title) && $shortcode_title != '' && isset($pageid) && $pageid != '') {
                $collection_query = DB::table('page_customize_table')
                                ->join('zestard_page_field', 'zestard_page_field.field_id', '=', 'page_customize_table.field_id')
                                ->join('zestard_global_config', 'zestard_global_config.store_id', '=', 'page_customize_table.store_id')
                                ->select('page_customize_table.*', 'zestard_page_field.*', 'zestard_global_config.*')
                                ->where(['zestard_page_field.shortcode_title' => $shortcode_title, 'page_customize_table.page_id' => $pageid, 'page_customize_table.store_id' => $shopId])->get();
                if (count($collection_query) > 0) {
                    return $collection_query;
                } else {
                    return 'error';
                }
            } else {
                return 'error';
            }
        } else {
            return 'err';
        }
    }

}
