<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class HelpController extends Controller {
    
    public function index()
    {
        $app_type = "Pages";
        $app_type_singular = "Page";
        $dashboard_route = "/shopifyapp/page-additional-fields/public/pages";
        $liquid_file_path = "/admin/themes/current/?key=templates/page.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_pages" id="{{page.id}}"></div></div>&#13;&#10;<div class="custom_page_fields" id="shortcode_page_field_title"> <b>[label]</b> : [value]</div>&#13;&#10;</div>';
        $field_shortcode = '<div class="custom_page_fields" id="shortcode_page_field_title"> <b>[label]</b> : [value]</div>';
        return view('help.help', compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
    
    public function appConfiguration()
    {
        $app_type = "Pages";
        $app_type_singular = "Page";
        $dashboard_route = "/shopifyapp/page-additional-fields/public/pages";
        $liquid_file_path = "/admin/themes/current/?key=templates/page.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_pages" id="{{page.id}}"></div></div>&#13;&#10;<div class="custom_page_fields" id="shortcode_page_field_title"> <b>[label]</b> : [value]</div>&#13;&#10;</div>';
        $field_shortcode = '<div class="custom_page_fields" id="shortcode_page_field_title"> <b>[label]</b> : [value]</div>';
        return view('app_configuration',compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
}