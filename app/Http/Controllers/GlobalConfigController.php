<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App;
use DB;

class GlobalConfigController extends Controller {

    public function index() {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);        
        $globalConfigArray = DB::table('zestard_global_config')->where('store_id', $select_store[0]->id)->get();
        if(count($globalConfigArray) > 0 ) { $globalConfigArray = $globalConfigArray; }else{ $globalConfigArray = 0; }
        return view('global_config',['data'=>$globalConfigArray]);
    }

    public function save_form_global_config(Request $request) {
        
        $data =  $request->input();        
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        
        $globalConfigArray = DB::table('zestard_global_config')->where('store_id', $select_store[0]->id)->get();
        $dataArray['date_field_order'] = serialize($data['date_field_order']);
        $dataArray['store_id'] = $select_store[0]->id;
        $dataArray['time_format'] = $data['time_format'];
        $dataArray['app_status'] = $data['app_status'];
        $dataArray['additional_css'] = trim($data['additional_css']);        
        $dataArray['additional_title'] = trim($data['additional_title']);
        if (count($globalConfigArray) != NULL) {
            DB::table('zestard_global_config')->where('store_id', $select_store[0]->id)->update($dataArray);
            $notification = array(
                'message' => 'Your setting has been update successfully',
                'alert-type' => 'success'
            );
        } else {
            DB::table('zestard_global_config')->insert($dataArray);
            $notification = array(
                'message' => 'Your setting has been add successfully',
                'alert-type' => 'success'
            );
        }
        return redirect('globalconfig')->with('notification', $notification);        
    }

}
